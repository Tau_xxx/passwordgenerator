<?php
/**
 * Generate password.
 */
if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])
    and strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
    header('Content-Type: application/json');

    // password generate function
    function randomPassword($length)
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $special = '.-+=_,!@$#*%<>[]{}';

        //echo json_encode($_POST['special']);die();

        if (isset($_POST['special']) and $_POST['special'] == '1') {
            $alphabet .= $special; //echo   json_encode($alphabet);die;
        }

        $pass = []; //remember to declare $pass as an array

        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        $pass = implode('', $pass);
        $pass = htmlentities($pass);

        return $pass;

        //return implode($pass); //turn the array into a string
    }

    if (isset($_POST['amount']) and $_POST['amount'] <= 45 and $_POST['amount'] >= 5) {
        $length = $_POST['amount'];
    } else {
        $length = 12;
    }

    $data = [];
    for ($i = 1; $i <= 12; $i++) {
        //$data[] = '<span>'.randomPassword($length).'</span>';

        $data[] = randomPassword($length);

        //$data[] =   htmlentities(randomPassword($length),ENT_QUOTES);
    }

    // передаем массив данных в json
    //$data=implode('',$data);

    usleep(500000);
    echo json_encode($data);
    exit;
}
