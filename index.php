<!DOCTYPE HTML>
<html>
<head>
<title>Генератор паролей онлайн. Taustyle</title>
<meta name="description" content="Онлайн генератор паролей. Taustyle.">
<meta name="msapplication-starturl" content="/passwordgenerator/">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="stylesheet" href="css/style.css">

</head>
<body>
<div id="container">
    <div id="message"><i style="margin-right: .5em;" class="fa fa-check" aria-hidden="true"></i>Пароль скопирован</div>
    <header>
        <h1>Генератор паролей онлайн</h1>
        <p>Всем известно, что пароль должен быть сложным, дабы злоумышленник не смог его подобрать. Не важно, социальные
            ли это сети Вконтакте, Фейсбук или Одноклассники, равно как и другие сервисы, безопасность всегда остается
            на первом месте. Cервис дает возможность любому желающему совершенно бесплатно сгенерировать и в дальнейшем
            использовать пароль для решения своих задач. Интерфейс максимально простой, пароли получаются действительно
            <span style="text-decoration: line-through">сочными</span> сложными.</p>
    </header>
    <div id="generator">
        <label>Двигайте ползунок,&nbsp;чтобы задать сложность пароля</label>
        <div id="soft" style="margin:20px 0 50px 0;width:100%;float:left;"></div>

        <div class="checkbox">
            <input id="check" type="checkbox" checked name="special" value="">
            <label for="check">Суперстойкий пароль (добавить символы <b>.-+=_,!@$#*%<>[]{}</b> )</label>
        </div>
          <label for="digit">
        <input type="number"
               name="amount"
               value="12"
               id="digit"
               class="amount___"
               placeholder="12"
               maxlength="2"
               min="5"
               max="45"></label>
        <input type="button" name="submit" class="submit_code" value="Cгенерировать пароль">
        <div class="wrap_result">

            <div class="result"></div>

            <div class="log">
                <div class="spinner">
                    <div class="dot1"></div>
                    <div class="dot2"></div>
                </div>
            </div>
        </div>

    </div> <!--.generator-->

<div class="new_feature">
   <!-- здесь будет новая опция.       -->
</div>
    <footer>&copy; Генератор паролей онлайн <?php echo date('Y'); ?>.</footer>
</div> <!--.container-->


<link rel="stylesheet" type='text/css' href="//cdnjs.cloudflare.com/ajax/libs/normalize/4.1.1/normalize.min.css">
<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Fira+Sans:400,700&subset=latin,cyrillic'>
<link rel="stylesheet" type='text/css' href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<link rel="stylesheet" type='text/css' href="//cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.4.0/nouislider.min.css">

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.4.0/nouislider.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.10/clipboard.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>