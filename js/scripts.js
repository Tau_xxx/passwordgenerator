$(document).ready(function () {



    // noUiSlider

    // add array for slider line

    var slider_start = 5;
    var slider_end = 45;
    var increase = 5;
    var arr = [];

    for (var i = slider_start; i <= slider_end; i += increase) {
        arr.push(i);
    }

    // create slider

    var softSlider = document.getElementById('soft');

    noUiSlider.create(softSlider, {
        start: 16,
        //step: 1,
        connect: 'lower',
        animate: true,
        range: {
            min: 0,
            max: 50
        },
        tooltips: false,
        pips: {
            mode: 'values',
            values: arr,
            density: 1
        }
    });

    // limit values

    softSlider.noUiSlider.on('change', function (values, handle) {
        if (values[handle] < 5) {
            softSlider.noUiSlider.set(5);
        } else if (values[handle] > 45) {
            softSlider.noUiSlider.set(45);
        }
    });


    // events for input
    var inputFormat = document.getElementById('digit');

    softSlider.noUiSlider.on('update', function (values, handle) {
        inputFormat.value = Math.round(values[handle]);
    });

    softSlider.noUiSlider.on('update ', function (values, handle) {
        var value = values[handle];

        if (handle) {
            inputFormat.value = value;
        }
    });

    inputFormat.addEventListener('change', function () {
        softSlider.noUiSlider.set(this.value);
    });

    generate_block_id = "#generator";


    //    ajax for password generate
    function generate_passwd(generate_block_id) {
        $(document).ajaxStart(function () {
            $(".log").css("visibility", "visible");
            $(".log").css("opacity", 1);
            $(".result").css("opacity", 0);
            $(".submit_code").attr("disabled", "disabled");
        });

        $.ajax({
            url: "generator.php",
            type: "POST",
            /**/
            beforeSend: function () {
                console.log(
                    $(generate_block_id + " input:checked").length
                )
            },
            cache: false,
            data: {
                amount: $(generate_block_id + ' .amount___').val(),
                special: $(generate_block_id + ' input:checked').length
            },
            dataType: "json",
            success: function (data) {

                // create html for array
                var content = '';

                $.each(data, function (i, post) {
                    content += '<span>' + post + '</span>';
                });
                $(generate_block_id + ' .result').html(content);
                // visuality ))
                $(".log").css("visibility", "");
                $(".log, .result").css("opacity", "");
                //$(".result").css("opacity", "");
                $(".submit_code").removeAttr("disabled");
            },
            complete: function (data) {
                console.log('complete');
            }

        });
    }


    $(generate_block_id + ' .submit_code').click(function () {
        generate_passwd(generate_block_id);
    })


    jQuery(window).load(function () {
        generate_passwd(generate_block_id);
    });


    // type only number
    $('#generator').on('keydown', 'input[type="number"]', function (e) {
        -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
    });


    // limit length
    function maxLengthCheck(object) {
        if (object.value.length > object.maxLength)
            object.value = object.value.slice(0, object.maxLength)
    }

    $('#generator').on('keyup', 'input[type="number"]', function () {
        maxLengthCheck(this);
    });


    // add to clipboard

    new Clipboard('.result span', {
        target: function (trigger) {
            return trigger;
            return trigger.nextElementSibling;
        }
    });

    // show message  add to clipboard
    $('.result').on('click', 'span', function () {
        $("#message").css('top', '0');

        setTimeout(function () {
            $("#message").css('top', '');
        }, 2500);
    });

}) // end jQuery